import lsqfit
import numpy as np
import gvar as gv

class fitter(object):

    def __init__(self, n_states, prior, t_start, t_end,
                correlators_gv, particle_statistics, t_period=None):

        if t_period is None and particle_statistics == 'bose-einstein':
            # Basically, this gets the length of the correlators gvar objects
            # Since the ps and ss should have the same length, we just use whichever
            # key is first in the dictionary
            t_period  = len(correlators_gv[correlators_gv.keys()[0]])

        if t_end is None:
            t_end = t_period - t_start

        self.t_period = t_period
        self.n_states = n_states
        self.t_start = t_start
        self.t_end = t_end
        self.particle_statistics = particle_statistics
        self.prior = prior
        self.correlators_gv = correlators_gv

        self.fit = None

    def get_fit(self):
        if self.fit is not None:
            return self.fit
        else:
            return self._make_fit()

    def get_energies(self):
        # Don't rerun the fit if it's already been made
        if self.fit is not None:
            temp_fit = self.fit
        else:
            temp_fit = self.get_fit()

        output = gv.gvar(np.zeros(self.n_states))
        output[0] = temp_fit.p['E0']
        for k in range(1, self.n_states):
            output[k] = output[0] + np.sum([(temp_fit.p['dE'][j]) for j in range(k)], axis=0)
        return output

    def _make_fit(self):
        # LOGIC FOR SIMULTANEOUS FITS:
        # This is the convoluted way we use MultiFitter
        # Essentially: first we create a model (which is a subclass of MultiFitter)
        # Then we make a fitter using the models
        # Finally, we make the fit with our two sets of correlators

        models = self._make_models_simult_fit()

        fitter = lsqfit.MultiFitter(models=models)
        fit = fitter.lsqfit(data=self.correlators_gv,
                            prior=self._make_prior(self.prior))
        self.fit = fit
        return fit

    def _make_models_simult_fit(self):
        models = np.array([])

        if self.particle_statistics == 'fermi-dirac':
            for key in self.correlators_gv.keys():
                param_keys = {
                    'E0'      : 'E0',
                    'log(dE)' : 'log(dE)',
                    'wf'      : 'wf_'+key,
                }
                models = np.append(models,
                           baryon_model(key, t=range(self.t_start, self.t_end),
                           param_keys=param_keys, n_states=self.n_states))

        if self.particle_statistics == 'bose-einstein':
            for key in self.correlators_gv.keys():
                param_keys = {
                    'log(E0)' : 'log(E0)',
                    'log(dE)' : 'log(dE)',
                    'wf'      : 'wf_'+key,
                }
                models = np.append(models,
                           meson_model(key, t=range(self.t_start, self.t_end), t_period = self.t_period,
                           param_keys=param_keys, n_states=self.n_states))
        return models

    # Converts normally-distributed energy priors to log-normally-distributed priors,
    # thereby forcing each excited state to be positive
    # and larger than the previous excited state
    def _make_prior(self, prior):
        resized_prior = {}
        for key in prior.keys():
            resized_prior[key] = prior[key][:self.n_states]
        new_prior = resized_prior.copy()

        if self.particle_statistics=='bose-einstein':
            # We force the energy to be positive by using the log-normal dist of E
            # let log(E) ~ eta; then E ~ e^eta
            E0_mean = gv.mean(resized_prior['E'][0])
            E0_std = gv.sdev(resized_prior['E'][0])

            # Here we pick mu, sigma st when we exponentiate the gvar log(E0) object,
            # the mean is equal to the mean of E from the prior we fed-in
            sigma = np.sqrt(np.log((E0_std / E0_mean)**2 + 1))
            mu = np.log(E0_mean / np.exp(sigma**2 / 2))
            new_prior['log(E0)'] = gv.gvar(mu, sigma)

        elif self.particle_statistics=='fermi-dirac':
            new_prior['E0'] = resized_prior['E'][0]

        # Don't need this entry
        new_prior.pop('E', None)

        # We force the energy to be positive by using the log-normal dist of dE
        # let log(dE) ~ eta; then dE ~ e^eta
        new_prior['log(dE)'] = gv.gvar(np.zeros(len(resized_prior['E']) - 1))
        for j in range(len(new_prior['log(dE)'])):

            # Notice that I've coded this s.t.
            # the std is determined entirely by the excited state
            dE_mean = gv.mean(resized_prior['E'][j+1] - resized_prior['E'][j])
            dE_std = gv.sdev(resized_prior['E'][j+1])

            # Here we pick mu, sigma st when we exponentiate the gvar log(E0) object,
            # the mean is equal to the mean of E from the prior we fed-in
            sigma = np.sqrt(np.log((dE_std / dE_mean)**2 + 1))
            mu = np.log(dE_mean / np.exp(sigma**2 / 2))

            new_prior['log(dE)'][j] = gv.gvar(mu, sigma)

        return new_prior


# This class is needed to instantiate an object for lsqfit.MultiFitter
# Used for particles that obey fermi-dirac statistics
class baryon_model(lsqfit.MultiFitterModel):
    def __init__(self, datatag, t, param_keys, n_states):
        super(baryon_model, self).__init__(datatag)
        # variables for fit
        self.t = np.array(t)
        self.n_states = n_states

        # keys (strings) used to find the wf_overlap and energy in a parameter dictionary
        self.param_keys = param_keys

    def fitfcn(self, p, t=None):
        if t is None:
            t = self.t

        wf = p[self.param_keys['wf']]
        E0 = p[self.param_keys['E0']]
        dE = np.exp(p[self.param_keys['log(dE)']])

        output = wf[0] * np.exp(-E0 * t)
        for j in range(1, self.n_states):
            E_j = E0 + np.sum([dE[k] for k in range(j)], axis=0)
            output = output + wf[j] * np.exp(-E_j * t)

        return output

    # The prior determines the variables that will be fit by multifitter --
    # each entry in the prior returned by this function will be fitted
    def buildprior(self, prior, mopt=None, extend=False):
        # Extract the model's parameters from prior.
        return prior

    def builddata(self, data):
        # Extract the model's fit data from data.
        # Key of data must match model's datatag!
        return data[self.datatag][self.t]

    def fcn_effective_mass(self, p, t=None):
        if t is None:
            t=self.t

        return np.log(self.fitfcn(p, t) / self.fitfcn(p, t+1))

# Used for particles that obey bose-einstein statistics
class meson_model(lsqfit.MultiFitterModel):
    def __init__(self, datatag, t, t_period, param_keys, n_states):
        super(meson_model, self).__init__(datatag)
        # variables for fit
        self.t = np.array(t)
        self.t_period = t_period
        self.n_states = n_states

        # keys (strings) used to find the wf_overlap and energy in a parameter dictionary
        self.param_keys = param_keys

    def fitfcn(self, p, t=None):

        if t is None:
            t = self.t

        wf = p[self.param_keys['wf']]
        E0 = np.exp(p[self.param_keys['log(E0)']])
        dE = np.exp(p[self.param_keys['log(dE)']])

        output = wf[0] * np.cosh( E0 * (t - self.t_period/2.0) )
        for j in range(1, self.n_states):
            E_j = E0 + np.sum([dE[k] for k in range(j)], axis=0)
            output = output + wf[j] * np.cosh( E_j * (t - self.t_period/2.0) )

        return output

    # The prior determines the variables that will be fit by multifitter --
    # each entry in the prior returned by this function will be fitted
    def buildprior(self, prior, mopt=None, extend=False):
        return prior

    def builddata(self, data):
        # Extract the model's fit data from data.
        # Key of data must match model's datatag!
        return data[self.datatag][self.t]

    def fcn_effective_mass(self, p, t=None):
        if t is None:
            t=self.t

        return np.arccosh((self.fitfcn(p, t-1) + self.fitfcn(p, t+1))/(2*self.fitfcn(p, t)))
